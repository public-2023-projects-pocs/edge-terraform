locals {
  env= "${terraform.workspace == "default"? "not use default workspace, instead use 'dev', 'staging', 'prod'": terraform.workspace }"

  sc_project_names = {
    dev     = "Edge-Dev"
    staging = "Edge-Dev"
    prod    = "Edge-Dev"
  }
  environments = {
    dev     = "development"
    staging = "staging"
    prod    = "production"
  }

  sc_profiles_pool = {
    dev  = [
      {
        context = "project"
        name    = "dev-cluster-infra-ubuntu-rke2-1-4-0-pro"
        tag     = "1.0.2"
      },{
        context = "project"
        name    = "dev-packs-infra"
        tag     = "1.1.0"
      },{
        context = "project"
        name    = "dev-k8s-addons"
        tag     = "1.0.4"
      },{
        context = "project"
        name    = "dev-ds-infra"
        tag     = "1.0.0"
      },{
        context = "project"
        name    = "dev-secrets"
        tag     = "1.0.0"
      },{
        context = "project"
        name    = "dev-services-manifests-v1"
        tag     = "1.0.1"
      }
    ]
    staging = [
      {
        context = "project"
        name    = "dev-cluster-infra-ubuntu-rke2-1-4-0-pro"
        tag     = "1.0.2"
      },{
        context = "project"
        name    = "staging-packs-infra"
        tag     = "1.0.0"
      },{
        context = "project"
        name    = "staging-k8s-addons"
        tag     = "1.0.2"
      },{
        context = "project"
        name    = "staging-ds-infra"
        tag     = "1.0.0"
      },{
        context = "project"
        name    = "staging-secrets"
        tag     = "1.0.0"
      },{
        context = "project"
        name    = "staging-services-manifests-v1"
        tag     = "1.0.1"
      }
    ]    
    prod = [
      {
        context = "project"
        name    = "prod-cluster-infra-ubuntu-rke2"
        tag     = "1.0.0"
      },{
        context = "project"
        name    = "production-packs-infra"
        tag     = "1.0.0"
      },{
        context = "project"
        name    = "production-k8s-addons"
        tag     = "1.0.0"
      },{
        context = "project"
        name    = "production-ds-infra"
        tag     = "1.0.0"
      },{
        context = "project"
        name    = "production-secrets"
        tag     = "1.0.0"
      },{
        context = "project"
        name    = "production-services-manifests-v1"
        tag     = "1.0.0"
      }
    ]
  }
  # Selection stage
  sc_project_name = "${lookup( local.sc_project_names, local.env)}"
  environment     = "${lookup( local.environments,     local.env)}"
  sc_profiles     = "${lookup( local.sc_profiles_pool, local.env)}"
}