##################################################################################
# Spectro Cloud credentials
##################################################################################

variable "sc_host" {
  type = string
}

variable "sc_api_key" {
  type = string
}

##################################################################################
# Cluster config
##################################################################################

variable "edge_location" {
  type = object({
    latitude  = string
    longitude = string
  })
}

variable "virtual_ip" {
  type = string
}

variable "appliance_status" {
  type    = string
  default = "ready"
}