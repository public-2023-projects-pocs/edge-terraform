data "spectrocloud_appliances" "appliances" {
  tags = {
    "auto-install" = true
    "env" = local.env
  }
  status       = var.appliance_status # default: ready, optional: in-use
  health       = "healthy"
  architecture = "amd64"
}

data "spectrocloud_appliance" "autoinstall" {
  for_each = toset(data.spectrocloud_appliances.appliances.ids)
  name = each.key
} 

output "appliances_filter_by_tags" {
  value = data.spectrocloud_appliance.autoinstall
}

module "edge-demo-module" {
  source  = "spectrocloud/edge/spectrocloud"
  version = "1.2.1"
  skip_wait_for_completion = true

  for_each = data.spectrocloud_appliance.autoinstall
  name = "edge-${lower(each.value.name)}"

  # Cluster VIP to be used with KubeVIP
  cluster_vip = var.virtual_ip

  cluster_tags = [
    "env:${local.env}", 
    "team:${coalesce(each.value.tags.team, "edge")}"
  ]
  
  # Machine Pools for Cluster
  # Control Plane Node Pool  
  machine_pools = [{
    name          = "control-plane"
    control_plane = true
    control_plane_as_worker = true
    edge_host = [{
      host_uid = each.value.id
    }]
  }]

  rbac_bindings = [{
    rbac_type = "ClusterRoleBinding"
    rbac_role = {
      "name" = "cluster-admin"
      "kind" = "ClusterRole"
    }
    subjects = [{
      name = "Edge-Admin"
      rbac_type = "Group"
    }]
  }]

  # Profiles to be added Profile should be an Edge-Native Infra or Full Profile with the OS, Kubernetes Distribution and CNI of choice
  cluster_profiles = local.sc_profiles

  # Cluster Geolocation (Optional)
  location = {
    latitude  = var.edge_location.latitude
    longitude = var.edge_location.longitude
  }
}

removed {
  from = module.edge-demo-module

  lifecycle {
    destroy = false
  }
}