<!-- BEGIN_TF_DOCS -->
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | >= 1.3.9 |
| <a name="requirement_spectrocloud"></a> [spectrocloud](#requirement\_spectrocloud) | = 0.16.1 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_spectrocloud"></a> [spectrocloud](#provider\_spectrocloud) | 0.16.1-pre |

## Modules

| Name | Source | Version |
|------|--------|---------|
| <a name="module_edge-demo-module"></a> [edge-demo-module](#module\_edge-demo-module) | spectrocloud/edge/spectrocloud | 1.2.1 |

## Resources

| Name | Type |
|------|------|
| [spectrocloud_appliance.autoinstall](https://registry.terraform.io/providers/spectrocloud/spectrocloud/0.16.1/docs/data-sources/appliance) | data source |
| [spectrocloud_appliances.appliances](https://registry.terraform.io/providers/spectrocloud/spectrocloud/0.16.1/docs/data-sources/appliances) | data source |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_appliance_status"></a> [appliance\_status](#input\_appliance\_status) | n/a | `string` | `"ready"` | no |
| <a name="input_edge_location"></a> [edge\_location](#input\_edge\_location) | n/a | <pre>object({<br>    latitude  = string<br>    longitude = string<br>  })</pre> | n/a | yes |
| <a name="input_sc_api_key"></a> [sc\_api\_key](#input\_sc\_api\_key) | n/a | `string` | n/a | yes |
| <a name="input_sc_host"></a> [sc\_host](#input\_sc\_host) | n/a | `string` | n/a | yes |
| <a name="input_virtual_ip"></a> [virtual\_ip](#input\_virtual\_ip) | n/a | `string` | n/a | yes |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_appliances_filter_by_tags"></a> [appliances\_filter\_by\_tags](#output\_appliances\_filter\_by\_tags) | n/a |
<!-- END_TF_DOCS -->